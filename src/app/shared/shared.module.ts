import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BaseurlInterceptor} from './interceptors/baseurl.interceptor';
import {ApiService} from './services/api.service';
import {ComplainService} from './services/complain.service';
import {ComplainModule} from './components/complain/complain.module';
import {ErrorListModule} from './error-list/error-list.module';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ComplainModule,
    ErrorListModule,
    InfiniteScrollModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BaseurlInterceptor,
      multi: true
    },
    ApiService,
    ComplainService
  ],
  exports: [
    CommonModule,
    InfiniteScrollModule
  ]
})
export class SharedModule {
}
