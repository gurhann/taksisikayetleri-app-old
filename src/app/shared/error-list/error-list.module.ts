import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ErrorListComponent} from './error-list.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ErrorListComponent
  ],
  exports: [
    ErrorListComponent
  ]
})
export class ErrorListModule {

}
