import {Component, Input, OnInit} from '@angular/core';
import {Error} from '../models/error.model';

@Component({
  selector: 'app-error-list',
  templateUrl: './error-list.component.html',
  styleUrls: ['./error-list.component.scss']
})
export class ErrorListComponent implements OnInit {

  @Input()
  errorList: Array<Error>;

  constructor() {
  }

  ngOnInit() {
  }

}
