import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'summaryComplainConent'
})
export class SummaryComplainContentPipe implements PipeTransform {

  transform(value: string, exponent: string): string {
    return value.substring(0, 200);
  }
}
