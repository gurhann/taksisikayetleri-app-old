import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';
import {Complain} from '../models/complain.model';
import {HttpParams} from '@angular/common/http';
import {HttpParamsOptions} from '@angular/common/http/src/params';
import {SearchComplainModel} from '../models/search-complain.model';

@Injectable()
export class ComplainService {

  constructor(private apiService: ApiService) {
  }

  getComplains(page: number, size: number): Observable<Array<Complain>> {

    const queryObject = {
        page: page.toString(),
        size: size.toString(),
        sort: 'createDate,desc',
        status
      }
    ;

    const params = new HttpParams({
      fromObject: queryObject
    });

    return this.apiService.get('/complains', params);
  }


  getComplain(slug: string): Observable<Complain> {
    return this.apiService.get(`/complains/${slug}`);
  }

  searchComplain(searchComplain: SearchComplainModel): Observable<Array<Complain>> {
    const params = new HttpParams({
      fromObject: {
        status: searchComplain.status,
        startDate: searchComplain.startDate,
        endDate: searchComplain.endDate,
        email: searchComplain.email
      }
    });
    return this.apiService.get('/complains/admin/search/', params, true);
  }

  reportComplain(complain: Complain): Observable<Complain> {
    return this.apiService.post('/complains', complain);
  }

  updateComplainStatus(uuid: string, status: string): Observable<Complain> {
    return this.apiService.put('/complains/admin/updateStatus', {
      uuid, status
    }, true);
  }

  uploadImage(slug: string, files: Array<File>): Observable<any> {
    const formdata: FormData = new FormData();
    files.forEach(file => {
      formdata.append('files', file, file.name);
    });
    formdata.append('slug', slug);
    return this.apiService.postForUpload('/uploadImage', formdata);
  }

}
