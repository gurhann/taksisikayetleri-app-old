import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs-compat/add/operator/map';
import {AuthService} from '../../admin/auth/auth.service';

@Injectable()
export class ApiService {

  constructor(private http: HttpClient,
              private authService: AuthService) {
  }

  private setHeaders(securityEnable: boolean): HttpHeaders {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    if (securityEnable) {
      headersConfig['Authorization'] = `Bearer ${this.authService.securityParams.access_token}`;
    }

    return new HttpHeaders(headersConfig);
  }

  get(path: string, params: HttpParams = new HttpParams(), securityEnable: boolean = false): Observable<any> {
    return this.http.get(path, {headers: this.setHeaders(securityEnable), params: params}).map(resp => {
      this.checkError(resp);
      return resp;
    });
  }

  put(path: string, body: Object = {}, securityEnable: boolean = false): Observable<any> {
    return this.http.put(
      path,
      body,
      {headers: this.setHeaders(securityEnable)}
    ).map(resp => {
      this.checkError(resp);
      return resp;
    });
  }

  post(path: string, body: Object = {}, securityEnable: boolean = false): Observable<any> {
    return this.http.post(
      path,
      body,
      {headers: this.setHeaders(securityEnable)}
    ).map(resp => {
      this.checkError(resp);
      return resp;
    });
  }

  delete(path, securityEnable: boolean = false): Observable<any> {
    return this.http.delete(
      path,
      {headers: this.setHeaders(securityEnable)}
    ).map(resp => {
      this.checkError(resp);
      return resp;
    });
  }

  postForUpload(path: string, body: Object = {}): Observable<any> {
    return this.http.post(
      path,
      body
    ).map(resp => {
      this.checkError(resp);
      return resp;
    });
  }

  private checkError(resp : any){
    if (resp && resp['errors']) {
      throw resp;
    }
  }

}
