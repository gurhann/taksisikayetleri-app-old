export interface SearchComplainModel {
  status: string;
  startDate: string;
  endDate: string;
  email: string;
}
