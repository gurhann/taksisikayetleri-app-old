export class Complain {
  uuid: string;
  slug: string;
  title: string;
  content: string;
  occurDate: Date;
  imageList: [{
    uuid: string
  }];
  createDate: Date;
  reporterName: string;
  reporterLastname: string;
  reporterPhone: string;
  reporterEmail: string;
  complainStatus: string;
}
