import {Component, Input, OnInit} from '@angular/core';
import {Complain} from '../../../models/complain.model';
import {AuthService} from '../../../../admin/auth/auth.service';

@Component({
  selector: 'app-summary-complain',
  templateUrl: './summary-complain.component.html',
  styleUrls: ['./summary-complain.component.scss']
})
export class SummaryComplainComponent implements OnInit {

  @Input() complain: Complain;

  constructor(public authService: AuthService) {
  }

  ngOnInit() {
  }

}
