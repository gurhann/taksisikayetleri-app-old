import {Component, Input, OnInit} from '@angular/core';
import {Complain} from '../../../models/complain.model';
import {ComplainService} from '../../../services/complain.service';
import {ActivatedRoute, ActivatedRouteSnapshot} from '@angular/router';

import 'rxjs/add/operator/mergeMap';
import {NgxGalleryAnimation, NgxGalleryImage, NgxGalleryOptions} from 'ngx-gallery';
import {environment} from '../../../../../environments/environment';
import {AuthService} from '../../../../admin/auth/auth.service';

@Component({
  selector: 'app-detail-complain',
  templateUrl: './detail-complain.component.html',
  styleUrls: ['./detail-complain.component.scss']
})
export class DetailComplainComponent implements OnInit {

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[] = [];

  complain: Complain;

  constructor(private complainService: ComplainService,
              private activatedRoute: ActivatedRoute,
              public authService: AuthService) {
  }

  ngOnInit() {
    this.configureGallery();
    const slug = this.activatedRoute.snapshot.params['slug'];
    this.complainService.getComplain(slug).subscribe(complain => {
      this.complain = complain;
      this.fetcImages();

    });
  }

  approveComplain() {
    this.changeStatusComplain('APPROVED');
  }

  rejectComplain() {
    this.changeStatusComplain('REJECTED');
  }

  changeStatusComplain(status: string) {
    this.complainService.updateComplainStatus(this.complain.uuid, status).subscribe(complain => {
      this.complain = complain;
    });
  }

  fetcImages() {
    this.complain.imageList.forEach(image => {
      this.galleryImages.push({
        small: `${environment.api_url}/showImage/${image.uuid}`,
        medium: `${environment.api_url}/showImage/${image.uuid}`,
        big: `${environment.api_url}/showImage/${image.uuid}`
      });
    });
  }

  configureGallery() {
    this.galleryOptions = [
      {
        width: '600px',
        height: '400px',
        thumbnails: true,
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      // max-width 800
      {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 400
      {
        breakpoint: 400,
        preview: false
      }
    ];

  }
}
