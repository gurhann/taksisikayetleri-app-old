import {Component, OnInit,ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ComplainService} from '../../../services/complain.service';
import {Complain} from '../../../models/complain.model';
import {Observable} from 'rxjs';
import {Error} from '../../../models/error.model';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/of';
import {FileHolder,ImageUploadComponent} from 'angular2-image-upload';

@Component({
  selector: 'app-report-complain',
  templateUrl: './report-complain.component.html',
  styleUrls: ['./report-complain.component.scss']
})
export class ReportComplainComponent implements OnInit {
  @ViewChild('imageInput') imageInput: ImageUploadComponent;
  reportComplainForm: FormGroup;
  errors: Array<Error>;
  uploadedFiles: Array<FileHolder> = [];

  constructor(private fb: FormBuilder,
              private complainService: ComplainService) {
  }

  ngOnInit() {
    this.initReportComplainForm();
  }

  private initReportComplainForm() {
    this.reportComplainForm = this.fb.group({
      title: this.fb.control(null, [Validators.required, Validators.maxLength(200)]),
      content: this.fb.control(null, [Validators.required]),
      occurDate: this.fb.control(null, [Validators.required]),
      userInfo: this.fb.group({
        name: this.fb.control(null, [Validators.required, Validators.maxLength(50)]),
        lastname: this.fb.control(null, [Validators.required, Validators.maxLength(50)]),
        email: this.fb.control(null, [Validators.required, Validators.email]),
        phone: this.fb.control(null)
      })
    });
  }

  onSubmit() {
    const complain = new Complain();
    complain.title = this.reportComplainForm.get('title').value;
    complain.content = this.reportComplainForm.get('content').value;
    const dateJson = this.reportComplainForm.get('occurDate').value;
    complain.occurDate = new Date(dateJson.year + '-' + dateJson.month + '-' + dateJson.day);
    complain.reporterName = this.reportComplainForm.get('userInfo').get('name').value;
    complain.reporterLastname = this.reportComplainForm.get('userInfo').get('lastname').value;
    complain.reporterEmail = this.reportComplainForm.get('userInfo').get('email').value;
    complain.reporterPhone = this.reportComplainForm.get('userInfo').get('phone').value;
  
    this.complainService.reportComplain(complain).mergeMap(
      c => {
        if (this.uploadedFiles.length >0)
          return this.complainService.uploadImage(c.slug, this.uploadedFiles.map(u => u.file));
        else
          return Observable.of([]);
      }
    ).subscribe(
      (data) => {
        this.reportComplainForm.reset();
        this.uploadedFiles = [];
        this.imageInput.deleteAll();
      }, err => {
        this.errors = err.error;
      }
    );
  }

  onUploadFinished(file: FileHolder) {
    this.uploadedFiles.push(file);
  }

  removedImage(file: FileHolder) {
    this.uploadedFiles = this.uploadedFiles.filter(f => !file);
  }

}
