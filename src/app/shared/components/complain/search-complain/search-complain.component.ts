import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ComplainService} from '../../../services/complain.service';

@Component({
  selector: 'app-search-complain',
  templateUrl: './search-complain.component.html',
  styleUrls: ['./search-complain.component.scss']
})
export class SearchComplainComponent implements OnInit {

  isCollapsed = true;
  searchComplainForm: FormGroup;

  @Output()
  searchResultsChange = new EventEmitter();

  @Output()
  searchFormClear = new EventEmitter();


  constructor(private fb: FormBuilder,
              private complainService: ComplainService) {

  }

  ngOnInit() {
    this.initSearchComplainForm();
  }

  private initSearchComplainForm() {
    this.searchComplainForm = this.fb.group({
      status: this.fb.control(null),
      startDate: this.fb.control(null),
      endDate: this.fb.control(null),
      email: this.fb.control(null, [Validators.email])
    });
  }

  onSubmit() {
    const startDateJson = this.searchComplainForm.value.startDate;
    const endDateJson = this.searchComplainForm.value.endDate;

    let startDate = '';
    let endDate = '';
    let email = '';

    if (startDateJson) {
      startDate = `${startDateJson.year}-${startDateJson.month}-${startDateJson.day}`;
    }

    if (endDateJson) {
      endDate = `${endDateJson.year}-${endDateJson.month}-${endDateJson.day}`;
    }

    if (this.searchComplainForm.value.email) {
      email = this.searchComplainForm.value.email;
    }

    this.complainService.searchComplain({
      status: this.searchComplainForm.value.status,
      startDate,
      endDate,
      email
    }).subscribe(complains => {
      this.searchResultsChange.emit(complains.slice());
    });
  }

  clearSearchForm() {
    this.searchComplainForm.reset();
    this.searchFormClear.emit(true);
  }

}
