import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComplainComponent } from './search-complain.component';

describe('SearchComplainComponent', () => {
  let component: SearchComplainComponent;
  let fixture: ComponentFixture<SearchComplainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchComplainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComplainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
