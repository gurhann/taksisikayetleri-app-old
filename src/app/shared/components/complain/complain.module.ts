import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SummaryComplainComponent} from './summary-complain/summary-complain.component';
import {ReportComplainComponent} from './report-complain/report-complain.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {InternationalPhoneModule} from 'ng4-intl-phone';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ImageUploadModule} from 'angular2-image-upload';
import {DetailComplainComponent} from './detail-complain/detail-complain.component';
import {NgxGalleryModule} from 'ngx-gallery';
import {SummaryComplainContentPipe} from '../../pipes/summary-complain-content.pipe';
import {ErrorListModule} from '../../error-list/error-list.module';
import {SearchComplainComponent} from './search-complain/search-complain.component';

const complainRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'report-complain',
    component: ReportComplainComponent
  },
  {
    path: 'complains/:slug',
    component: DetailComplainComponent
  }
]);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    complainRouting,
    InternationalPhoneModule,
    NgbModule,
    ImageUploadModule,
    NgxGalleryModule,
    complainRouting,
    ErrorListModule
  ],
  declarations: [
    SummaryComplainComponent,
    ReportComplainComponent,
    DetailComplainComponent,
    SearchComplainComponent,
    SummaryComplainContentPipe
  ],
  exports: [
    SummaryComplainComponent,
    SearchComplainComponent
  ]
})
export class ComplainModule {
}
