import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  @ViewChild('loginForm') loginForm: NgForm;


  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
  }

  onLoggedin() {
    this.authService.getAccessToken(this.loginForm.value.username, this.loginForm.value.password).subscribe((params) => {
      this.router.navigate(['/admin/']);
    });

  }
}
