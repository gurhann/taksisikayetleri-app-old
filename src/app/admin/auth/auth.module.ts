import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AuthComponent} from './auth.component';
import {AuthService} from './auth.service';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

const authRoutes: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'login',
    component: AuthComponent
  }
]);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    authRoutes
  ],
  declarations: [AuthComponent],
  providers: [AuthService],
  exports: []
})
export class AuthModule {
}
