import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {SecurityParams} from '../../shared/models/security-params.model';
import {Router} from '@angular/router';
@Injectable()
export class AuthService {

  securityParams: SecurityParams;

  constructor(private http: HttpClient,public router: Router) {
  }

  getAccessToken(username: string, password: string) {
    return this.http.post<SecurityParams>(`/oauth/token?grant_type=password&username=${username}&password=${password}`,
      null, {headers: this.getRequestHeaders()})
      .map((data: SecurityParams) => {
        this.securityParams = data;
      });
  }

  revokeAccessToken(){
          this.securityParams = undefined;
          this.router.navigate(['/']);       
  }


  private getRequestHeaders(): HttpHeaders {
    return new HttpHeaders({
      'authorization': 'Basic ' + btoa(`${environment.server_username}:${environment.server_password}`),
      'cache-control': 'no-cache',
      'content-type': 'application/x-www-form-urlencoded; charset=utf-8'
    });
  }

}
