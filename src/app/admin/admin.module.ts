import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthModule} from './auth/auth.module';
import {RouterModule} from '@angular/router';
import {CanActivateViaOauthGuard} from './oauth.canactivateguard';
import {DashboardModule} from './dashboard/dashboard.module';
import {DashboardComponent} from './dashboard/dashboard.component';

const adminRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'admin',
    redirectTo: 'admin/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'admin/dashboard',
    component: DashboardComponent,
    canActivate: [CanActivateViaOauthGuard]
  }
]);

@NgModule({
  imports: [
    CommonModule,
    AuthModule,
    DashboardModule,
    adminRouting
  ],
  exports: [AuthModule, DashboardModule],
  providers: [CanActivateViaOauthGuard]
})

export class AdminModule {
}
