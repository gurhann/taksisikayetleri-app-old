import {CanActivate, Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {AuthService} from './auth/auth.service';

@Injectable()
export class CanActivateViaOauthGuard implements CanActivate {

  constructor(public router: Router, private authService: AuthService) {
  }

  canActivate() {
    if(!this.authService.securityParams) {
      this.router.navigateByUrl('/login');
    }
    return (this.authService.securityParams === null) ? false : true;
  }
}
