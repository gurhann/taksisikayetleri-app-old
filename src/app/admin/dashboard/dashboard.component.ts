import {Component, OnInit} from '@angular/core';
import {ComplainService} from '../../shared/services/complain.service';
import {Complain} from '../../shared/models/complain.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  scrollDistance = 0.05;

  private page = 0;
  private itemSize = 5;
  private isSearchResults = false;

  complains: Array<Complain> = [];


  constructor(private complainService: ComplainService) {
  }

  ngOnInit() {
    this.addItem();
  }

  onScrollDown(ev) {
    console.log('scrolled down!!', ev);

    if (this.isSearchResults) {
      return;
    }
    // add another 20 items
    this.page++;
    this.addItem();
  }

  addItem() {
    this.complainService.getComplains(this.page, this.itemSize).map(resp => resp['content']).subscribe(complains => {
      this.complains = this.complains.concat(complains.slice());
    });

  }

  setComplains(complains: Array<Complain>) {
    this.complains = complains;
    this.isSearchResults = true;
  }

  clearedSearchForm(cleared: boolean) {
    if (cleared) {
      this.complains = [];
      this.isSearchResults = false;
      this.page = 0;
      this.addItem();
    }
  }

}
