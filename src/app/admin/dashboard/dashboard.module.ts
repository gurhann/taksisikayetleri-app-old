import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {ComplainModule} from '../../shared/components/complain/complain.module';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ComplainModule,
    SharedModule
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule {
}
