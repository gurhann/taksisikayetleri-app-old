import {NgModule} from '@angular/core';
import {LayoutModule} from './layout/layout.module';
import {SharedModule} from '../shared/shared.module';
import {DashboardModule} from './dashboard/dashboard.module';

@NgModule({
  imports: [
    SharedModule,
    DashboardModule,
    LayoutModule
  ],
  exports: [
    DashboardModule,
    LayoutModule
  ]
})
export class CoreModule {
}
