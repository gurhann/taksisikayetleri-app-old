import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../admin/auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit() {
  }

  onLoggedout() {
    this.authService.revokeAccessToken();
  }
}
