import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SocialIconsComponent} from './social-icons/social-icons.component';
import {TopHeaderComponent} from './top-header/top-header.component';
import {TopNavComponent} from './top-nav/top-nav.component';
import {FooterComponent} from './footer/footer.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    SocialIconsComponent,
    TopHeaderComponent,
    TopNavComponent,
    FooterComponent],
  exports: [
    SocialIconsComponent,
    TopHeaderComponent,
    TopNavComponent,
    FooterComponent]
})
export class LayoutModule {
}
