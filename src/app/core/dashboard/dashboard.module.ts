import {ModuleWithProviders, NgModule} from '@angular/core';
import {DashboardComponent} from './dashboard.component';
import {SharedModule} from '../../shared/shared.module';
import {ComplainModule} from '../../shared/components/complain/complain.module';
import {RouterModule} from '@angular/router';

const dashboardRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: '',
    component: DashboardComponent
  }
])
@NgModule({
  imports: [
    ComplainModule,
    SharedModule,
    dashboardRouting,
  ],
  declarations: [DashboardComponent],
  exports: [
    RouterModule
  ]
})
export class DashboardModule { }
