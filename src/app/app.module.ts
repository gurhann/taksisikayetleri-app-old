import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, ModuleWithProviders, NgModule} from '@angular/core';
import {registerLocaleData} from '@angular/common';
import localeTR from '@angular/common/locales/tr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';  
import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {NgbDateParserFormatter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateFRParserFormatter} from './shared/ngb-date-fr-parser-formatter';
import {ImageUploadModule} from 'angular2-image-upload';
import {AdminModule} from './admin/admin.module';
import {CoreModule} from './core/core.module';

const rootRouting: ModuleWithProviders = RouterModule.forRoot([
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  }
], {useHash: false});

registerLocaleData(localeTR);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    AdminModule,
    rootRouting,
    NgbModule.forRoot(),
    ImageUploadModule.forRoot(), 
    BrowserAnimationsModule,
  ],
  providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter},
    {provide: LOCALE_ID, useValue: 'tr'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
